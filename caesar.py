import random
import pyperclip as buffer
from colorama import Fore, Style


red_bold_text = Style.BRIGHT + Fore.RED
green_bold_text = Style.BRIGHT + Fore.GREEN
close = Style.RESET_ALL
bright = Style.BRIGHT


def key_generator():
    key = random.randint(0, 11)
    return key


def encoder(source_msg, key):
    encrypt_msg = ''
    for current_symbol in source_msg:
        encrypt_msg += chr(ord(current_symbol) + key)

    buffer.copy(encrypt_msg)
    return encrypt_msg


def decoder(encrypt_msg, key):
    decrypt_msg = ''
    for current_symbol in encrypt_msg:
        decrypt_msg += chr(ord(current_symbol) - key)

    buffer.copy(decrypt_msg)
    return decrypt_msg


def write_to_file(msg_for_writing):
    file = open('./result.txt', 'w')

    try:
        file.write(msg_for_writing)

    except PermissionError:
        print(red_bold_text + 'No write permission' + close)
        print('Message not written to file\n\n')

    else:
        print(green_bold_text + 'Message written to "result.txt"\n\n' + close)

    finally:
        file.close()


while(True):
    answer = input('\n\nEnter "e" for encode or "d" for decode: ')

    if answer == 'e':
        source_msg = input('\nEnter message to encrypt: ')
        key = key_generator()
        encrypt_msg = encoder(source_msg, key)
        print('\nEncrypted message: ' + green_bold_text + encrypt_msg + close)
        print(green_bold_text + 'Message copied to clipboard' + close)
        print(bright + 'Key for decode: ' + str(key) + close)

        check_on_write = input('\nDo you want to write the result to a file?\
                                \nEnter ' + bright + 'y/n' + close + ': ')
        if check_on_write == 'y':
            write_to_file(encrypt_msg)
        else:
            print('\n')

        break

    elif answer == 'd':
        source_msg = input('\nEnter message to decrypt: ')
        key = int(input('Enter key: '))
        decrypt_msg = decoder(source_msg, key)
        print('\nDecrypted message: ' + green_bold_text + decrypt_msg + close)
        print(green_bold_text + 'Message copied to clipboard' + close)

        check_on_write = input('\nDo you want to write the result to a file?\
                                \nEnter ' + bright + 'y/n' + close + ': ')
        if check_on_write == 'y':
            write_to_file(decrypt_msg)
        else:
            print('\n')

        break

    else:
        print(red_bold_text + 'Wrong command.' + close)
        print('Enter "' + green_bold_text + 'e' + close +
              '" or "' + green_bold_text + 'd' + close + '"')
